package org.electroblanzifor.mnemo;

import android.view.animation.Interpolator;

/**
 * Created by ayal on 04/10/13.
 */
public class BounceSin implements Interpolator {
    public BounceSin() {
    }

    private static float bounce(float t) {
        return t * t * 8.0f;
    }

    public float getInterpolation(float t) {
        // _b(t) = t * t * 8
        // bs(t) = _b(t) for t < 0.3535
        // bs(t) = _b(t - 0.54719) + 0.7 for t < 0.7408
        // bs(t) = _b(t - 0.8526) + 0.9 for t < 0.9644
        // bs(t) = _b(t - 1.0435) + 0.95 for t <= 1.0
        // b(t) = bs(t * 1.1226)
        return 1-(float)Math.abs(Math.sin(((Math.pow(t,2.1f))*3.14f)*7f))*(1-t);
    }
}
