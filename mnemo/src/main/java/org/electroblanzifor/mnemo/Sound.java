package org.electroblanzifor.mnemo;

import android.content.Context;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import android.util.SparseIntArray;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**************************************************************************************************/

/**
 * Created by Boob on 24/09/13.
 */
public class Sound
{
    public static int STREAM = AudioManager.STREAM_MUSIC;
    public static String[] VOICES = {"abs_sine", "little_guy"};

    private static Sound _instance;

    private SoundPool pool = null;
    private int intro_id = 0;
    private SparseIntArray sound_ids = new SparseIntArray();
    private HashMap<String,Voice> voices = new HashMap<String, Voice>();
    //private Voice[] voices = new Voice[VOICES.length];

    private Sound(Context context) {
        // initialize sound pool
        pool = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);

        // load assets
        AssetManager assets = context.getAssets();
        try {
            intro_id = pool.load(assets.openFd("audio/intro.wav"), 1);
            /*for(String voice_name : VOICES) {
                voices.put(voice_name, new Voice(pool, assets, voice_name));
            }*/
            for (int i=57; i<=69; i++) {
                int id = pool.load(assets.openFd("audio/little_guy/"+i+".wav"), 1);
                sound_ids.put(i, id);
            }
        } catch (IOException e) {
            Log.i("silly_birds", "Exception! " + e.toString());
        }
    }

    public static Sound instance(Context context) {
        if (_instance == null) {
            _instance = new Sound(context);
        }
        return _instance;
    }

    /**********************************************************************************************/

    class Voice
    {
        public String name;
        public int[] range;
        private SoundPool pool;
        private SparseIntArray sound_ids = new SparseIntArray();

        public Voice(SoundPool pool_, AssetManager assets, String name_) throws IOException {
            name = name_;
            range = read_range(assets, name);
            pool = pool_;
            for (int i=range[0]; i<=range[1]; i++) {
                int id = pool.load(assets.openFd("audio/"+name+"/"+i+".wav"), 1);
                sound_ids.put(i, id);
            }
        }
        private int[] read_range(AssetManager assets, String name) throws IOException {
            InputStream input = assets.open("audio/"+name+"/range.json");
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();
            String json = new String(buffer);

            try {
                JSONArray json_arr = new JSONArray(json);
                int from = json_arr.getInt(0);
                int to = json_arr.getInt(1);
                return new int[]{from, to};
            } catch (JSONException e) {
                throw new IOException("can't read range of voice "+name);
            }
        }
        /*****************************************************************************/
        public void note_on(int pitch) {
            Log.i("silly_birds", "playing note "+pitch+"...");
            pool.play(sound_ids.get(pitch), 1, 1, 0, 0, 1);
        }
    }

    /**********************************************************************************************/

    /*public void close() {
        pool.release();
    }*/

    public void intro() {
        Log.i("silly_birds", "playing intro...");
        pool.play(intro_id, 1, 1, 0, 0, 1);
    }

    public void note_on(int pitch) {
        Log.i("silly_birds", "playing note "+pitch+"...");
        pool.play(sound_ids.get(pitch), 1, 1, 0, 0, 1);
    }

    public void panic() {
        pool.autoPause();
    }
}
