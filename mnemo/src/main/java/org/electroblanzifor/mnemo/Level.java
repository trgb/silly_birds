package org.electroblanzifor.mnemo;

/**************************************************************************************************/

/**
 * Created by Boob on 24/09/13.
 */
public class Level {

    public final int id;
    public final String name;
    public final ViewConfig viewconf;
    public final Stimulus[] stimuli;
    public final Stimulus[] hapaxes;
    //public final int n_stimuli;
    public final int timeout;
    public final int energy;

    public Level(int id_, String name_, ViewConfig viewconf_, Stimulus[] stimuli_, Stimulus[] hapaxes_, int timeout_, int energy_) {
        id = id_;
        name = name_;
        viewconf = viewconf_;
        stimuli = stimuli_;
        hapaxes = hapaxes_;
        //n_stimuli = stimuli.length;
        timeout = timeout_;
        energy = energy_;
    }

    public Level(int id_, String name_, ViewConfig viewconf_, Stimulus[] stimuli_) {
        this(id_, name_, viewconf_, stimuli_, new Stimulus[]{}, 0, 3);
    }

    public Level(int id_, String name_, ViewConfig viewconf_, Stimulus[] stimuli_, int energy_) {
        this(id_, name_, viewconf_, stimuli_, new Stimulus[]{}, 0, energy_);
    }
}
