package org.electroblanzifor.mnemo;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

/**
 * Created by ayal on 15/09/13.
 */
public class LevelButton extends RelativeLayout {
    private final int x;
    private final int y;
    private final int r;
    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //public final Stimulus stimulus;
    private final MenuActivity context;
    private final Level level_data;
    private final int level;


    public LevelButton(MenuActivity context, int x, int y, int r, int color, int level, Level level_data) {
        super(context);
        Log.i("WebViewDemo", "LevelButton-1");

        mPaint.setColor(color);
        Log.i("WebViewDemo", "LevelButton-2");
        this.context = context;
        this.x = x;
        this.y = y;
        this.r = r;
        //this.stimulus = new Stimulus(this, sound);
        this.level = level;
        this.level_data = level_data;
        Log.i("WebViewDemo", "LevelButton-3");

        // eggs in menu item:
        Statistics.LevelInfo li = Statistics.instance(context).get_level_info(level_data.id);
        Log.i("WebViewDemo", "playcount for level " + level + " = " + li.play_count);
        if (li.play_count > 0) {
          int mistakes = li.best_score;
          Log.i("WebViewDemo", "mistakes for level " + level + " = " + li.best_score);

            int score = Game.energy_to_score(level_data.energy, mistakes);
            Log.i("WebViewDemo", "real score for level " + level + " = " + score);
            ScoreView sv = new ScoreView(context, level_data.energy, score, 0, 0);

            RelativeLayout.LayoutParams scoreparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            scoreparams.leftMargin = (int)(GameData.scale * 5);
            scoreparams.topMargin = (int)(GameData.scale * 5);
            sv.setPadding(50,50,0,0);
            sv.show(this, scoreparams);

            ScaleAnimation s = new ScaleAnimation(0.0f, 0.42f, 0.0f, 0.42f, 75, 50);
            s.setDuration(600);
            s.setInterpolator(new CustomBounceInterpolator());
            s.setFillEnabled(true);
            s.setFillAfter(true);
            sv.startAnimation(s);
        }

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int eventaction = event.getAction();
                Log.i("WebViewDemo", "ON-TOUCH Menu button!");
                if (eventaction == MotionEvent.ACTION_UP) {
                    // Do what you want
                    return true;
                }
                if (eventaction == 0) {
                    ((LevelButton) v).goToLevel();
                    return true;
                }
                return false;
            }
        });
    }

    private void goToLevel() {
        try {
            Log.i("WebViewDemo", "going to level " + this.level);
            //GameData.setLevel(this.level);
            Intent intent = new Intent(context, GameActivity.class);
            intent.putExtra("level_num", this.level);
            context.startActivity(intent);
        }
        catch (Exception yikes) {
            Log.i("WebViewDemo", "Exception: " + yikes.toString());
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint p = new Paint();
        p.setColor(Color.WHITE);
        p.setTextSize(30);
        canvas.drawText("" + level, 20 ,80 ,p);
    }
}
