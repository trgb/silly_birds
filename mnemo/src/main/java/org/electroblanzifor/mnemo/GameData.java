package org.electroblanzifor.mnemo;

/**************************************************************************************************/

import android.graphics.Point;

import java.util.HashMap;

/**
 * Created by Boob on 24/09/13.
 */
public class GameData
{
    public static int C = Stimulus.MIDDLE_C;

    public static Stimulus A3  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C-3);
    public static Stimulus Bb3 = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C-2);
    public static Stimulus B3  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C-1);
    public static Stimulus C4  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C);
    public static Stimulus Cs4 = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+1);
    public static Stimulus D4  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+2);
    public static Stimulus Ds4 = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+3);
    public static Stimulus E4  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+4);
    public static Stimulus F4  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+5);
    public static Stimulus Fs4 = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+6);
    public static Stimulus G4  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+7);
    public static Stimulus Gs4 = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+8);
    public static Stimulus A4  = new Stimulus(new int[]{0},  Stimulus.MIDDLE_C+9);

    public static int[] m2nd   = new int[]{0,1};
    public static int[] M2nd   = new int[]{0,2};
    public static int[] m3rd   = new int[]{0,3};
    public static int[] M3rd   = new int[]{0,4};
    public static int[] P4th   = new int[]{0,5};
    public static int[] A4th   = new int[]{0,6};
    public static int[] P5th   = new int[]{0,7};
    public static int[] m6th   = new int[]{0,8};
    public static int[] M6th   = new int[]{0,9};
    public static int[] m7th   = new int[]{0,10};
    public static int[] M7th   = new int[]{0,11};

    public static int[] M   = new int[]{0,4,7};
    public static int[] m   = new int[]{0,3,7};
    public static int[] dim = new int[]{0,3,6};
    public static int[] Aug = new int[]{0,4,8};

    public static int[] Q3P  = new int[]{0,5,10};
    public static int[] Q3l  = new int[]{0,6,11};
    public static int[] Q3u  = new int[]{0,5,11};

    public static Stimulus S(int[] pitches, int bass) {
        return new Stimulus(pitches, bass);
    }

    public static Stimulus C4_M   = new Stimulus(M, Stimulus.MIDDLE_C);
    public static Stimulus C4_m   = new Stimulus(m, Stimulus.MIDDLE_C);
    public static Stimulus C4_dim = new Stimulus(dim, Stimulus.MIDDLE_C);
    public static Stimulus C4_Aug = new Stimulus(Aug, Stimulus.MIDDLE_C);

    /**********************************************************************************************/

    public static float scale;

    public static BirdConfig bird1 = new BirdConfig(R.drawable.silly1, new Point(20,90));
    public static BirdConfig bird2 = new BirdConfig(R.drawable.silly2, new Point(50,85));
    public static BirdConfig bird3 = new BirdConfig(R.drawable.silly3, new Point(50,80));
    public static BirdConfig bird4 = new BirdConfig(R.drawable.silly4, new Point(50,87));
    public static BirdConfig bird5 = new BirdConfig(R.drawable.silly5, new Point(50,90));
    public static BirdConfig bird6 = new BirdConfig(R.drawable.silly6, new Point(100,160), 2);

    public static ViewConfig conf = new ViewConfig(
            new Scaffold(
                new Point[]{
                            new Point(90,334),
                            new Point(170,493),
                            new Point(329,670),
                            new Point(613,427),
                            new Point(300,834),
                            new Point(534,256),
                            new Point(504,427),
                            new Point(711,604),
                            new Point(611,604),
                            new Point(511,604),
                            new Point(300,493),
                            new Point(275,334),
                            new Point(90,670),
                            new Point(512,770),
                            new Point(704,427),
                            new Point(200,670)},
                R.drawable.treez),
            new BirdConfig[]{
                    bird1,
                    bird2,
                    bird3,
                    bird4,
                    bird5,
                    bird6,
                    bird1,
                    bird2,
                    bird3,
                    bird4,
                    bird5,
                    bird1,
                    bird2,
                    bird3,
                    bird4,
                    bird5,
                    bird1,
                    bird2,
                    bird3},
            R.drawable.cloudz);

    /**********************************************************************************************/

    // TODO: assert uniqueness
    public static int ID(int id) { return id; }

    public static Level[] EASY_LEVELS = {
        //new Level("A: hapaxes 1a", conf, new Stimulus[]{A3}, new Stimulus[]{Cs4, E4, B3}),
        //new Level("A: hapaxes 1b", conf, new Stimulus[]{E4}, new Stimulus[]{A3, Cs4, B3}),
        //new Level("A: hapaxes 1c", conf, new Stimulus[]{A3, E4}, new Stimulus[]{Cs4, B3}),
        new Level(ID(0), "A: major triad", conf, new Stimulus[]{A3, Cs4, E4}, 4),
        //new Level("hapaxes 1d", conf, new Stimulus[]{A3, E4, Cs4}, new Stimulus[]{B3}),
        new Level(ID(1), "A: 4 notes from scale", conf, new Stimulus[]{A3, Cs4, E4, B3}, 4),
        new Level(ID(2), "A: 5 notes from scale", conf, new Stimulus[]{A3, Cs4, E4, B3, D4}),
        new Level(ID(3), "Intervals: fourths and fifths", conf, new Stimulus[]{S(P4th,C+2), S(A4th,C), S(P5th,C-3)}, 5),
        new Level(ID(4), "Intervals: thirds and sixths", conf, new Stimulus[]{S(m3rd,C+2), S(M3rd,C), S(m6th,C-3), S(M6th,C-2)}, 5),
        new Level(ID(5), "Triads: major, minor and dim. on C", conf, new Stimulus[]{C4_M, C4_m, C4_dim}, 5),
        new Level(ID(6), "Triads: all triads on C", conf, new Stimulus[]{C4_M, C4_m, C4_dim, C4_Aug}, 6),
        new Level(ID(7), "A: 6 notes from scale", conf, new Stimulus[]{A3, Cs4, E4, B3, D4, Fs4}, 6),
        new Level(ID(8), "A: 7 notes from scale", conf, new Stimulus[]{A3, Cs4, E4, B3, D4, Fs4, Gs4}, 5),
        new Level(ID(9), "Triads: all triads with soprano A", conf, new Stimulus[]{S(M,C+2), S(m,C+2), S(dim,C+3), S(Aug,C+1)}, 6),
        new Level(ID(10), "A: 3-note chromatic cluster", conf, new Stimulus[]{A3, Bb3, B3}, 6),
        new Level(ID(11), "A: 4-note chromatic cluster", conf, new Stimulus[]{A3, Bb3, B3, C4}, 6),
        new Level(ID(12), "A: 5-note chromatic cluster", conf, new Stimulus[]{A3, Bb3, B3, C4, Cs4}, 6),
        /*new Level("A: hapaxes 2a", conf, new Stimulus[]{A3}, new Stimulus[]{B3, Cs4, D4, E4, Fs4}),
        new Level("A: hapaxes 2b", conf, new Stimulus[]{E4}, new Stimulus[]{A3, B3, Cs4, D4, Fs4}),
        new Level("A: hapaxes 2c", conf, new Stimulus[]{A3, E4}, new Stimulus[]{B3, Cs4, Fs4}),
        new Level("A: hapaxes 2d", conf, new Stimulus[]{A3, E4, Cs4}, new Stimulus[]{B3, Fs4}),
        new Level("A: hapaxes 2e", conf, new Stimulus[]{A3, E4, Cs4, B3}, new Stimulus[]{Fs4}),*/
        new Level(ID(13), "3-note Quartals on A", conf, new Stimulus[]{S(Q3P,C-3), S(Q3l,C-3), S(Q3u,C-3)}, 6),
        new Level(ID(14), "3-note Quartals with soprano A", conf, new Stimulus[]{S(Q3P,C-1), S(Q3l,C-2), S(Q3u,C-2)}, 6),
        /*new Level("A: hapaxes 3a", conf, new Stimulus[]{A3}, new Stimulus[]{B3, Cs4, D4, E4, Fs4, Gs4}),
        new Level("A: hapaxes 3b", conf, new Stimulus[]{E4}, new Stimulus[]{A3, B3, Cs4, D4, Fs4, Gs4}),
        new Level("A: hapaxes 3c", conf, new Stimulus[]{A3, E4}, new Stimulus[]{B3, Cs4, Fs4, Gs4}),
        new Level("A: hapaxes 3d", conf, new Stimulus[]{A3, E4, Cs4}, new Stimulus[]{B3, Fs4, Gs4}),
        new Level("A: hapaxes 3e", conf, new Stimulus[]{A3, E4, Cs4, B3}, new Stimulus[]{Fs4, Gs4}),
        new Level("A: hapaxes 3f", conf, new Stimulus[]{A3, E4, Cs4, B3, Fs4}, new Stimulus[]{Gs4}),*/
        new Level(ID(15), "A: 6-note chromatic cluster", conf, new Stimulus[]{A3, Bb3, B3, C4, Cs4, D4}, 8),
        new Level(ID(16), "A: 7-note chromatic cluster", conf, new Stimulus[]{A3, Bb3, B3, C4, Cs4, D4, Ds4}, 8),
    };

    /**********************************************************************************************/

    public static HashMap<String, Level[]> LEVEL_CATEGORIES;
    static {
        LEVEL_CATEGORIES = new HashMap<String, Level[]>();
        LEVEL_CATEGORIES.put("Easy", EASY_LEVELS);
    }
}
