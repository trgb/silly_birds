package org.electroblanzifor.mnemo;

import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;

import java.util.HashMap;

/**************************************************************************************************/

/**
 * Created by Boob on 26/09/13.
 */
public class Game
{
    private class StimulusCounter {
        public int count=0;
        public boolean matched=false;
    }

    public final int n_cards;
    private final Level level;
    private final GameListener listener;

    private final Card[] cards;
    private final HashMap<Stimulus, StimulusCounter> stimucounters;

    public String status;
    private CountDownTimer countdown;
    private long start_time=0, end_time=0;

    private Card prev_card = null;
    private int match_count = 0;

    public Game(Level level_, GameListener listener_)
    {
        status = "unstarted";
        level = level_;
        listener = listener_;
        n_cards = level.stimuli.length*2 /*+ level.hapaxes.length*/;
        cards = new Card[n_cards];

        // add cards normal (double) stimuli
        stimucounters = new HashMap<Stimulus, StimulusCounter>();
        for (int i=0; i<level.stimuli.length; i++) {
            cards[i*2]   = new Card(level.stimuli[i]);
            cards[i*2+1] = new Card(level.stimuli[i]);
            stimucounters.put(level.stimuli[i], new StimulusCounter());
        }
        // add cards for hapaxes
        /*for (int j=0; j<level.hapaxes.length; j++) {
            cards[level.stimuli.length*2+j] = new Card(level.hapaxes[j]);
        }*/
        Utils.shuffle(cards);
    }

    /**********************************************************************************************/
    // controller API

    public Card[] get_cards() {
        return cards;
    }

    // start measuring time
    public void start()
    {
        countdown = new CountDownTimer(level.timeout*1000, 7) {
            public void onTick(long millisUntilFinished) {
                listener.on_tick(millisUntilFinished);
            }
            public void onFinish() {
                stop();
                status = "timeout";
                listener.on_timeout();
            }
        };

        if (level.timeout > 0)
            countdown.start();

        start_time = SystemClock.uptimeMillis();
        status = "unfinished";
    }

    // stop measuring time
    public void stop() {
        if (end_time == 0)
            end_time = SystemClock.uptimeMillis();
        countdown.cancel();
    }

    public void touch_card(Card card)
    {
        Card prev_card_ = prev_card;
        prev_card = card;

        if (prev_card_ != card && stimucounters.containsKey(card.stimulus)) {
            // count statistics
            // * but disregard consequent touches of same card
            // * count non-hapaxes only
            stimucounters.get(card.stimulus).count += 1;
        }

        if (prev_card_ != null &&
                !prev_card_.matched &&
                prev_card_ != card &&
                prev_card_.stimulus == card.stimulus)
        {
            // match detected!
            prev_card_.matched = true;
            card.matched = true;
            stimucounters.get(card.stimulus).matched = true;
            match_count++;

            // callbacks
            listener.on_match(prev_card_, card);
            if (match_count == level.stimuli.length) {
                stop();
                status = "success";
                listener.on_game_complete();
            }
        }

        // according to our score calculation it is
        // not possible for this happen immediately
        // after a game complete event.
        if (get_score() <= -level.energy) {
            stop();
            status = "scoreout";
            listener.on_scoreout();
        }
    }

    public static int energy_to_score(int initial_energy, int mistakes){
        int realScore = (int)Math.ceil( (initial_energy + mistakes) * 3.0f / initial_energy);
        Log.i("WebViewDemo",
                "RealScore: "  + realScore);
        return realScore;
    }

    /**********************************************************************************************/
    // statistics API

    public int get_score() {
        int score = 0;
        for (HashMap.Entry<Stimulus, StimulusCounter> entry : stimucounters.entrySet()) {
            Stimulus stimulus = entry.getKey();
            StimulusCounter counter = entry.getValue();

            // in order to be useful before the game's end, we must predict the minimum possible
            // future count per stimulus. if a stimulus is unmatched, at least one more click is
            // needed. if it is also not the previously clicked stimulus, at least two more clicks
            // are needed.
            int predicted_count = counter.count;
            if (!counter.matched) {
                predicted_count += 1;
                if (prev_card == null || prev_card.stimulus != stimulus)
                    predicted_count += 1;
            }
            if (predicted_count > 3)
                score += 3 - predicted_count;
        }
        Log.i("WebViewDemo", "get_score = " + score);
        return score;
    }

    public double get_time() {
        assert end_time != 0;
        return (end_time - start_time) / 1000.0;
    }
}
