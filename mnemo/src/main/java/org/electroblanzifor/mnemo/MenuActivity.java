package org.electroblanzifor.mnemo;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

import java.util.Random;

/**
 * Created by ayal on 23/09/13.
 */
public class MenuActivity extends Activity {

    private boolean didOnce;

    @Override
    public void onStart() {
        Log.i("WebViewDemo", "onStart");
        super.onStart();
        /*Log.i("WebViewDemo", "mid onStart");
        try {
            EasyTracker et = EasyTracker.getInstance(this);
            Log.i("WebViewDemo", "mid mid onStart");
            et.activityStart(this);  // Add this method.
        }
        catch (Exception yikes) {
            Log.i("WebViewDemo", "onStart/EasyTracker exception: " + yikes.toString());
        }*/
        Log.i("WebViewDemo", "end onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        Log.i("WebViewDemo", "METRIX " + metrics.density + " " + metrics.densityDpi);


        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        Log.i("WebViewDemo", "creating menu...");
        super.onCreate(savedInstanceState);
        Log.i("WebViewDemo", "menu 1");
        //new GameData(this);
        Log.i("WebViewDemo", "menu 2");
        setContentView(R.layout.activity_menu);
    }

    public static int iscale(int x){
        return (int)(GameData.scale * x);
    }

    public static int iscale(double x){
        return (int)(GameData.scale * x);
    }

    public static float scale(int x){
        return GameData.scale * x;
    }

    public void createMenu(RelativeLayout menu) {
        Log.i("WebViewDemo", "menu 2.1 " + menu);
        try {
            Log.i("WebViewDemo", "menu H/W");
        int width = menu.getWidth();
        int height = menu.getHeight();
            Log.i("WebViewDemo", "menu H/W :: " + width + "/" + height);
            int orig_treeheight = 1146; //bd.getBitmap().getHeight();
            int orig_treewidth = 800;//bd.getBitmap().getWidth();

           float hscale = ((float)height / (float)orig_treeheight);
            int newwidth = (int) ((float)orig_treewidth * hscale);
            int newheight = height;
            float scale = 1;

           /* if (newwidth > width) { // so we should scale by width
                scale = ((float)width / (float)orig_treewidth);
                newwidth = width;
                newheight = (int) ((float)orig_treeheight * scale);
                Log.i("sillybirds", "scale by width !!!!!!");
            }
            else {
                scale = hscale;
                Log.i("sillybirds", "scale by height !!!!!!");
            }
*/
            scale = 1;

        GameData.scale = scale;

        Random r = new Random();
        int y = 0;
        int x = 0;
        int col = 0;
        Log.i("WebViewDemo", "menu 2.2");
        for (int i = 0; i < GameData.EASY_LEVELS.length; i++) {
            int red = r.nextInt(255);
            int g = r.nextInt(255);
            int b = r.nextInt(255);
            int color = 0xff000000 + (red << 16) + (g << 8) + b;
            //if (i % ((int)(width / 120)) == 0 && i != 0) {
            int ducksinrow = 4;

            if ((i % ducksinrow) == 0 && i != 0) {
                y += 1;
                x = 0;
            }
            Log.i("WebViewDemo", "menu 2.3");
            createLevelButton(menu,
                    (int)((x + 1) * (width / (ducksinrow + 1)) - scale(75)) + (x*25 - 50),
                    (int)((y + 1) * (height / (Math.ceil((double) GameData.EASY_LEVELS.length / (double) ducksinrow) + 1)) - scale(75)),
                    iscale(220),
                    color,
                    i,
                    GameData.EASY_LEVELS[i],
                    i * 60);
            x += 1;
        }
        }
        catch (Exception yikes) {
            Log.i("WebViewDemo", "Exception: " + yikes.toString());
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        try {
            Log.w("sillybirds", "FOCUS!");
            if (hasFocus && !this.didOnce) {
                //this.didOnce = true;
                RelativeLayout menu = (RelativeLayout) findViewById(R.id.menu);
                if (menu != null) {
                    menu.setBackgroundResource(R.drawable.backgroundz);
                    createMenu(menu);
                }
                else {
                    Log.w("sillybirds", "no menu view");
                }
            }
        }
        catch (Exception e) {
            Log.w("sillybirds", "focus change exception:" + e.toString());
        }
    }

    private static void createLevelButton(RelativeLayout menu, int x, int y, int r, int color, int level, Level level_data, long soff){
        Log.i("WebViewDemo", "menu 2.3." + level);
        Statistics.LevelInfo li = Statistics.instance(menu.getContext()).get_level_info(level_data.id);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(r, r);
        params.leftMargin = x;
        params.topMargin = y;

        LevelButton lb = new LevelButton((MenuActivity)menu.getContext(), x, y, r, color, level, level_data);
        RoundRectShape rect = new RoundRectShape(
                new float[] {30,30, 30,30, 30,30, 30,30},
                null,
                null);

        ShapeDrawable bg = new ShapeDrawable(rect);
        if (li.play_count > 0) {
            bg.getPaint().setColor(Color.parseColor("#537A73"));
        }
        else {
            bg.getPaint().setColor(Color.parseColor("#9BBF6D"));
        }

        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            lb.setBackgroundDrawable(bg);
        } else {
            lb.setBackground(bg);
        }

        menu.addView(lb, params);

        Random rnd = new Random();
        ScaleAnimation s = new ScaleAnimation(0.0f, 0.8f, 0.0f, 0.8f, 50, 50);

        s.setDuration((rnd.nextInt(4) + 5) * 100);
        s.setInterpolator(new CustomBounceInterpolator());

        s.setFillEnabled(true);
        s.setFillAfter(true);
        lb.startAnimation(s);
    }

}
