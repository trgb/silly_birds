package org.electroblanzifor.mnemo;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by ayal on 03/10/13.
 */
public class ScoreView extends RelativeLayout {
    private Activity context;
    private final int initial_energy;
    private int energy;
    private int ofx = 0 ;
    private int ofy = 0;
    //private final Paint paint;
    private ArrayList<EggView> eggs = new ArrayList<EggView>();
    private int max_eggs = 3;
    //private int eggs;

    public ScoreView(Activity context, int energy) {
        super(context);
        this.context = context;
        this.energy = this.initial_energy = energy;
    }

    public ScoreView(Activity context, int energy, int size, int ofx, int ofy) {
        this(context, energy);
        max_eggs = size;
        this.ofx = ofx;
        this.ofy = ofy;
    }

    public void show(RelativeLayout main, RelativeLayout.LayoutParams params) {
        show(main, params, 1f);
    }

    public void show(RelativeLayout main, RelativeLayout.LayoutParams params, final float scaleview) {
        try {
        final ScoreView that = this;
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        for (int i = 0; i < max_eggs; i++) { // three eggs by default
                            int res = context.getResources().getIdentifier("egg_" + (i % 3), "drawable", "org.electroblanzifor.mnemo");
                            EggView egg = new EggView(context, res);
                            eggs.add(egg);
                            addEgg(egg, that.ofx + (int)(GameData.scale * ( EggView.w * i)),
                                    that.ofy + (int)(GameData.scale * 6),
                                    80 * i);
                        }
                    }
                }, 900);

        main.addView(this, params);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

        ScaleAnimation s = new ScaleAnimation(0.0f, scaleview, 0.0f, scaleview, 75, 50);
        s.setDuration(600);
        s.setInterpolator(new CustomBounceInterpolator());
        s.setFillEnabled(true);
        s.setFillAfter(true);
        that.startAnimation(s);
                    }
                },
        1000);

        }
        catch(Exception exx){
            Log.i("WebViewDemo", "view show Exception " +  exx);
        }

    }

    private void addEgg(EggView eggView, int x, int y, long offset) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(EggView.w, EggView.h);
        params.leftMargin = x;
        params.topMargin = y;
        this.addView(eggView, params);

        ScaleAnimation s = new ScaleAnimation(0.0f, GameData.scale * 1.0f, 0.0f, GameData.scale * 1.0f, GameData.scale * 50, GameData.scale * 50);
        s.setDuration(600);
        s.setInterpolator(new CustomBounceInterpolator());
        s.setFillEnabled(true);
        s.setFillAfter(true);
        s.setStartOffset(offset);
        eggView.startAnimation(s);
    }

    private void removeEgg(EggView egg) {
        ScaleAnimation s = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 50, 50);
        s.setDuration(600);
        s.setInterpolator(new CustomBounceInterpolator());
        s.setFillEnabled(true);
        s.setFillAfter(true);
        egg.startAnimation(s);

        eggs.remove(egg);
        this.removeView(egg);
    }

    public void update(int score) {
        Log.i("WebViewDemo", "set score: " + score);

        int map = Game.energy_to_score(energy, score);
        while  (map < eggs.size()) {
            EggView egg = eggs.get(map);
            removeEgg(egg);
        }
    }


    public void move_to_other_view(final RelativeLayout main, final DialogView dialog) {
        Log.i("WebViewDemo", "stealing eggs");
        ScaleAnimation s = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, GameData.scale * 75, GameData.scale * 50);
        s.setDuration(600);
        s.setInterpolator(new CustomBounceInterpolator());
        s.setFillEnabled(true);
        s.setFillAfter(true);
        final ScoreView that = this;
        main.post(new Runnable() {
            public void run() {
                context.runOnUiThread(new Runnable() {
                    public void run() {
                        main.removeView(that);

                        RelativeLayout.LayoutParams scoreparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        scoreparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        scoreparams.addRule(RelativeLayout.CENTER_VERTICAL);

                        //scoreparams.topMargin = 250;

                        Log.i("WebViewDemo", "adding eggs to dialog");
                        ScoreView sv = new ScoreView(context, energy, that.eggs.size(), 50, 50);
                        sv.show(dialog, scoreparams, 2f);

         /*               ScaleAnimation s = new ScaleAnimation(0.0f, 10.0f, 0.0f, 10.0f, 75, 50);
                        s.setDuration(600);
                        s.setInterpolator(new CustomBounceInterpolator());
                        s.setFillEnabled(true);
                        s.setFillAfter(true);
                        sv.startAnimation(s);*/
                    }
                });
            }});




        s.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });

        this.startAnimation(s);


    }
}