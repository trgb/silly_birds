package org.electroblanzifor.mnemo;

/**************************************************************************************************/

/**
 * Created by Boob on 26/09/13.
 */
public interface GameListener
{
    void on_tick(long millis_left);
    void on_match(Card card1, Card card2);
    void on_timeout();
    void on_scoreout();
    void on_game_complete();
}
