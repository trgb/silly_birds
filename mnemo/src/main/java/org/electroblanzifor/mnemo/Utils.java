package org.electroblanzifor.mnemo;

import java.util.Random;

/**
 * Created by ayal on 07/10/13.
 */
public class Utils {

    // Fisher–Yates shuffle (in-place)
    public static void shuffle(Object[] array) {
        Random rnd = new Random();
        for (int i = array.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            Object a = array[index];
            array[index] = array[i];
            array[i] = a;
        }
    }
}
