package org.electroblanzifor.mnemo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by ayal on 04/10/13.
 */
public class EggView extends View {
    private final Paint paint;
    private final int eggType;
    public static int w = 60;
    public static int h = 84;

    public EggView(Activity context, int eggType) {
        super(context);
        this.paint = new Paint(Paint.FILTER_BITMAP_FLAG |
                Paint.DITHER_FLAG |
                Paint.ANTI_ALIAS_FLAG);
        this.eggType = eggType;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), eggType);
        paint.setColor(Color.GREEN);
        canvas.drawBitmap(bmp, 0, 0, paint);
    }

}
