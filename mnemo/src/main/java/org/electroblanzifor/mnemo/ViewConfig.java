package org.electroblanzifor.mnemo;

public class ViewConfig {
    public final Scaffold scaffold;
    public final BirdConfig[] birds;
    public final int backgroundResource;

    public ViewConfig(Scaffold s, BirdConfig[] birds, int backgroundResource){
        this.scaffold = s;
        this.birds = birds;
        this.backgroundResource = backgroundResource;
    }
}
