package org.electroblanzifor.mnemo;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Created by ayal on 15/09/13.
 */
public class CardView extends View {
    private final int x;
    private final int y;
    private final int r;
    public final Card info;
    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //public final Stimulus stimulus;
    private final GameActivity context;
    private final BirdConfig bcfg;
    public String animating = "idle";
    private Bitmap bmp;
    private Canvas canvas;


    public CardView(GameActivity context, int x, int y, int r, Card info, BirdConfig bcfg, int color) {
        super(context);
        this.setDrawingCacheEnabled(true);
        //Log.i("WebViewDemo", "CardView-1");
        mPaint.setColor(color);
        //Log.i("WebViewDemo", "CardView-2");
        this.bcfg = bcfg;
        this.context = context;
        this.x = x;
        this.y = y;
        this.r = r;
        this.info = info;
        //this.stimulus = new Stimulus(this, sound);
        //Log.i("WebViewDemo", "CardView-3");
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int eventaction = event.getAction();
                //Log.i("WebViewDemo", "ON-TOUCH!, " + eventaction + " , sound >> " + ((CardView)v).stimulus.resourceName);
                if(eventaction == MotionEvent.ACTION_UP){
                 // Do what you want
                    return true;
                }
                if(eventaction == 0) {
                    try {
                        // if pixel is transparent not handling touch
                        // if not, handling touch and not letting other views handle it (i.e 'return true')

                        Log.i("WebViewDemo", "You touched " + getResources().getResourceName(((CardView)v).bcfg.rid));
                        Log.i("WebViewDemo", "event x,y is = " + event.getX() + " : " + event.getY());
                        Log.i("WebViewDemo", "bmp w,h is = " + ((CardView)v).bmp.getWidth() + " : " + ((CardView)v).bmp.getHeight());
                        Log.i("WebViewDemo", "scaled width " +
                                ((float)100 * (float)GameData.scale * (float)((CardView)v).bcfg.scale));

                        int pixel = ((CardView)v).bmp.getPixel((int)event.getX(),(int)event.getY());

                        Log.i("WebViewDemo", "TOUCH PIXEL >>> " + pixel);
                        if (pixel != 0) {
                            ((CardView)v).context.on_card_touch((CardView) v);
                            return true;
                        }
                    }
                    catch (Exception exx) {
                        Log.i("WebViewDemo", "TOUCH EXCEPTION " + exx);
                    }

                }
                return false;
            }
        });
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.buildDrawingCache();
        this.canvas = canvas;
        try {
        if (this.animating == "animating") {
            String resourceName = getResources().getResourceName(this.bcfg.rid);
            resourceName += "_smile";
            int rid = getResources().getIdentifier(resourceName, "drawable", "org.electroblanzifor.mnemo");
            this.bmp = BitmapFactory.decodeResource(getResources(), rid);
        }
        else if (this.animating == "idle") {
            this.bmp = BitmapFactory.decodeResource(getResources(), this.bcfg.rid);
        }
        else if (this.info.matched) {
            String resourceName = getResources().getResourceName(this.bcfg.rid);
            resourceName += "_end";
            int rid = getResources().getIdentifier(resourceName, "drawable", "org.electroblanzifor.mnemo");
            this.bmp = BitmapFactory.decodeResource(getResources(), rid);
        }

            Paint bpaint = new Paint(Paint.FILTER_BITMAP_FLAG |
                    Paint.DITHER_FLAG |
                    Paint.ANTI_ALIAS_FLAG);

        Bitmap b = Bitmap.createScaledBitmap(this.bmp,
                (int) ((float)100 * (float)GameData.scale * (float)bcfg.scale),
                (int) ((float)100 * (float)GameData.scale * (float)bcfg.scale), true);

         Canvas c = new Canvas(b);
         c.drawBitmap(b,0,0,bpaint);
         this.bmp = b;

         canvas.drawBitmap(b,0,0,bpaint);
        }
        catch(Exception exx){
            Log.i("WebViewDemo", "problem drawing " +  exx + " " + exx.getStackTrace());
        }
    }

    /**********************************************************************************************/
    // controller API

    public void ack_touch() {
        // acknowledge being touched
        //Log.i("silly_birds", "animating!");

        // change image:
        animating = "animating";
        invalidate();
        final CardView that = this;
        ScaleAnimation trans = new ScaleAnimation(1.0f, 1.00f, 1.0f, 1.00f);
        trans.setDuration(250);
        trans.setInterpolator(new AccelerateInterpolator(1.0f));
        trans.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i("WebViewDemo", "animation end");
                animating = "idle";
                invalidate();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        this.startAnimation(trans);

        // end animating
    }

    public void on_match() {
        // acknowledge being matched
        Animation a = new AlphaAnimation(1.0f, 0.3f);
        a.setDuration(150);
        a.setFillAfter(true);
        a.setFillEnabled(true);
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i("WebViewDemo", "animation end");
                animating = "end";
                invalidate();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(a);
    }

}
