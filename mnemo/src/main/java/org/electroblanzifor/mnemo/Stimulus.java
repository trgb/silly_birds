package org.electroblanzifor.mnemo;

/**************************************************************************************************/

/**
 * Created by Boob on 24/09/13.
 */
public class Stimulus {

    public static int MIDDLE_C = 60;

    public int[] pitches = null;
    public int bass = 0;

    public Stimulus(int[] pitches) {
        this(pitches, MIDDLE_C-3);
    }

    public Stimulus(int[] pitches, int bass) {
        this.pitches = pitches;
        this.bass = bass;
    }

    public void play(Sound sound) {
        for (int pitch : pitches) {
            sound.note_on(pitch + bass);
        }
    }
}
