package org.electroblanzifor.mnemo;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Random;

/**************************************************************************************************/

public class GameActivity extends Activity implements GameListener
{
    private static final String LOG_TAG = "silly_birds";
    //public ArrayList<Stimulus> moves = new ArrayList<Stimulus>();
    //private Stimulus lastMove;
    //private int level = 16;

    //public int completed = 0;
    //private CountDownTimer timer;

    private Sound sound = null;
    private Statistics stats = null;
    public int level_num = 0;
    private Level level = null;
    public Game game = null;
    private HashMap<CardView, Card> view_to_card = null;
    private HashMap<Card, CardView> card_to_view = null;
    private View tree;
    private boolean didOnce = false;
    private int width;
    private int height;
    public ScoreView scoreview;

    public void trackGame(){
        Statistics.LevelInfo level_info = stats.get_level_info(level.id);


        Log.i(LOG_TAG, "Tracking game:\nStatus: " + game.status + " " + level_info.to_json(level.name));

        /*EasyTracker.getInstance(this).send(MapBuilder
                .createEvent("Game",     // Event category (required)
                        game.status,  // Event action (required)
                        level_info.to_json(level.name),   // Event label
                        null)            // Event value
                .build()
        );

        EasyTracker.getInstance(this).send(MapBuilder
                .createEvent("Game",     // Event category (required)
                        "time",  // Event action (required)
                        level.name,   // Event label
                        (long)(game.get_time() * 1000))          // Event value
                .build()
        );

        EasyTracker.getInstance(this).send(MapBuilder
                .createEvent("Game",     // Event category (required)
                        "score",  // Event action (required)
                        level.name,   // Event label
                        (long)game.get_score())            // Event value
                .build()
        );
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
        */
    }

    @Override
    public void onStart() {
        super.onStart();
        //EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        game.stop(); // this is needed in case the game did not stop of itself
        trackGame();
//        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            // initialize sound
            sound = Sound.instance(getApplicationContext());
            setVolumeControlStream(Sound.STREAM);

            stats = Statistics.instance(getApplicationContext());

            // initialize level
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                level_num = extras.getInt("level_num");
                Log.i(LOG_TAG, "LEVEL: "+level_num);
            }
            else
                Log.i(LOG_TAG, "NO LEVEL SPECIFIED!");

            level = GameData.EASY_LEVELS[level_num];
            game = new Game(level ,this);
            view_to_card = new HashMap<CardView, Card>(game.n_cards);
            card_to_view = new HashMap<Card, CardView>(game.n_cards);


            /*this.timer = new CountDownTimer(GameData.currentLevel().time, 1) {
                public void onTick(long millisUntilFinished) {
                    try {
                        TextView textViewTimer = (TextView)findViewById(R.id.time);
                        if (textViewTimer != null) {
                            textViewTimer.setText("" + millisUntilFinished);
                        }
                    }
                    catch (Exception exx) {
                        Log.i(LOG_TAG, "Exception! " + exx.toString());
                    }
                }
                public void onFinish() {
                    end();
                }
            }.start();*/

            // initialize gui
            setContentView(R.layout.activity_main);


        }
        catch (Exception e) {
            Log.i(LOG_TAG, "Exception!");
            Log.e(LOG_TAG, e.toString());
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        try {
            if (hasFocus && !this.didOnce) {
                this.didOnce = true;
                RelativeLayout main = (RelativeLayout) findViewById(R.id.main);
                Log.i(LOG_TAG, "preparing layout...");
                prepare_layout(main);
                sound.intro();
                game.start();
            }
        }
        catch (Exception e) {
            Log.w(LOG_TAG, "focus change exception:" + e.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**********************************************************************************************/
    // layout

    private CardView create_card(RelativeLayout main, int x, int y, int r, Card card, BirdConfig bcfg, int color){
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(r, r);
        params.leftMargin = x;
        params.topMargin = y;
        CardView card_view = new CardView((GameActivity)main.getContext(), x, y, r, card, bcfg, color);
        main.addView(card_view, params);
        return card_view;
    }

    public void prepare_layout(RelativeLayout main) {
        TextView scoreText = (TextView)findViewById(R.id.score);
        //scoreText.setText("Score: " + GameData._score);
        //scoreText.setText("Score: n/a");
        scoreText.setText("");


        Log.i(LOG_TAG, "creating game level: >> " + level_num + " >> sounds " + game.n_cards);

        int width = 0;
        int height = 0;

        View content = getWindow().findViewById(Window.ID_ANDROID_CONTENT);
        Log.d(LOG_TAG, content.getWidth() + " x " + content.getHeight());
        Log.d(LOG_TAG, main.getWidth() + " x " + main.getHeight());
        width = content.getWidth();
        height = content.getHeight();

        Log.i(LOG_TAG, "drawable screen width/height " + width + ":" + height);

        BitmapDrawable bd = (BitmapDrawable) this.getResources().getDrawable(R.drawable.treez);
        Log.i(LOG_TAG, "TREE RESOURCE >>> " + bd.getBitmap().getHeight() + " :: " + bd.getBitmap().getWidth());

        int orig_treeheight = 1146; //bd.getBitmap().getHeight();
        int orig_treewidth = 800;//bd.getBitmap().getWidth();

        float hscale = ((float)height / (float)orig_treeheight);
        int newwidth = (int) ((float)orig_treewidth * hscale);
        int newheight = height;
        float scale = 0;

        if (newwidth > width) { // so we should scale by width
            scale = ((float)width / (float)orig_treewidth);
            newwidth = width;
            newheight = (int) ((float)orig_treeheight * scale);
            Log.i(LOG_TAG, "scale by width !!!!!!");
        }
        else {
            scale = hscale;
            Log.i(LOG_TAG, "scale by height !!!!!!");
        }

        GameData.scale = scale;

        int offsetx = (int)(((float)width - (float)newwidth) / 2f);
        int offsety = (int)(((float)height - (float)newheight));

        Log.i(LOG_TAG, "scale data >>> " +
                " scale = " + scale +
                " orig_treeheight: " +orig_treeheight +
                " orig_treewidth: " + orig_treewidth +
                " newheight: " + newheight +
                " newwidth: " + newwidth +
                " offsets " + offsetx + " :: " + offsety);

        // background:
        main.setBackgroundResource(R.drawable.backgroundz);

        final int finalWidth = width;
        final int finalHeight = height;

        // clouds:
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width * 2, height);
        params.leftMargin = -width;
        params.topMargin = 0;
        params.addRule(RelativeLayout.ALIGN_LEFT);

        main.addView(new ImageView(this){{
            setScaleType(ScaleType.FIT_XY);
            setImageResource(level.viewconf.backgroundResource);
            TranslateAnimation trans = new TranslateAnimation(0,finalWidth, 0,0);
            trans.setDuration(100000);
            trans.setRepeatCount(-1);
            trans.setRepeatMode(Animation.REVERSE);
            trans.setInterpolator(new LinearInterpolator());
            this.startAnimation(trans);
        }}, params);

        TextView textViewTimer = (TextView)findViewById(R.id.time);
        textViewTimer.setText(level.name);
        textViewTimer.setTextColor(Color.BLACK);
        textViewTimer.setTextSize(30);
        textViewTimer.bringToFront();

        // put tree
        params = new RelativeLayout.LayoutParams(newwidth, newheight);

        View imageView=new View(this);
        this.tree = imageView;
        imageView.setBackgroundResource(level.viewconf.scaffold.treeResource);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        main.addView(imageView, params);
        // put birds on tree
        Card[] cards = game.get_cards();
        Random r = new Random();
        int y = 0;
        int x = 0;
        Utils.shuffle(level.viewconf.scaffold.places);
        for (int i = 0; i < game.n_cards; i++) {
            int red = r.nextInt(255);
            int g = r.nextInt(255);
            int b = r.nextInt(255);
            int color = 0xff000000 + (red << 16) + (g << 8) + b;
            //if (i % ((int)(width / 120)) == 0 && i != 0) {
            CardView card_view = create_card(main,
                    (int) ((((float)level.viewconf.scaffold.places[i].x - (float)level.viewconf.birds[i].anchor.x) * scale)) + offsetx,
                    (int) ((((float)level.viewconf.scaffold.places[i].y - (float)level.viewconf.birds[i].anchor.y) * scale)) + offsety,
                    //(int) ((((float)level.viewconf.scaffold.places[i].x - (float)level.viewconf.birds[i].anchor.x) * scale)) + offsetx,
                    //(int) ((((float)level.viewconf.scaffold.places[i].y - (float)level.viewconf.birds[i].anchor.y) * scale)) + offsety,
                    250,
                    cards[i],
                    level.viewconf.birds[i],
                    color);

            view_to_card.put(card_view, cards[i]);
            card_to_view.put(cards[i], card_view);

            x += 1;
        }

        // scoreview (eggs):
        this.scoreview = new ScoreView(this, level.energy, 3, 150, 100);
        RelativeLayout.LayoutParams scoreparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        scoreparams.leftMargin = (int)(GameData.scale * 100);
        scoreparams.topMargin = (int)(GameData.scale * 100);
        scoreview.show(main, scoreparams, 2f);

    }

    /**********************************************************************************************/

    public void on_card_touch(CardView card_view) {
        //Log.i(LOG_TAG, "moving " + card_view);
        RelativeLayout main = (RelativeLayout) findViewById(R.id.main);
        /*Log.i(LOG_TAG, "main width/height " + main.getWidth() + ":" + main.getHeight());
        Log.i(LOG_TAG, "main padding " + main.getPaddingBottom() + ":" + main.getPaddingTop() + ":" + main.getPaddingLeft());

        Log.i(LOG_TAG, "tree width/height " + tree.getWidth() + ":" + tree.getHeight());
        Log.i(LOG_TAG, "tree padding " + tree.getPaddingBottom() + ":" + tree.getPaddingTop() + ":" + tree.getPaddingLeft());*/

        try {
            Card card = view_to_card.get(card_view);

            if (!card.matched) {
                card_view.ack_touch();
                card.stimulus.play(sound);
                game.touch_card(card);
            }

            scoreview.update(game.get_score());
        }
        catch (Exception exx) {
            Log.i(LOG_TAG, "touching exception: " + exx.toString());
        }
        //Log.i(LOG_TAG, "finished moving " + card_view);
    }

    /**********************************************************************************************/
    // game callbacks

    public void on_tick(long millis_left) {
        try {
            TextView textViewTimer = (TextView)findViewById(R.id.time);
            if (textViewTimer != null) {
                textViewTimer.setText("" + millis_left);
            }
        }
        catch (Exception exx) {
            Log.i(LOG_TAG, "Exception! " + exx.toString());
        }
    }

    public void on_match(Card card1, Card card2) {
        // update gui
        CardView card_view1 = card_to_view.get(card1);
        CardView card_view2 = card_to_view.get(card2);
        card_view1.on_match();
        card_view2.on_match();
        // TODO: make sound
    }

    public void on_timeout() {
        TextView textViewTimer = (TextView)findViewById(R.id.time);
        textViewTimer.setText("TIME OVER!");
    }

    public void on_scoreout()
    {
        Log.i("WebViewDemo", "game over..");

        RelativeLayout main = (RelativeLayout) findViewById(R.id.main);
        DialogView dialog = new DialogView(this, R.drawable.dialog_bad_egg, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, just close
                // the dialog box and restart level
                finish();
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra("level_num", level_num);
                startActivity(intent);
            }
        });
        dialog.show(main, "You lost all your eggs!", true);

    }

    public void on_game_complete() {
        Statistics.LevelInfo level_info = stats.update_level(level.id, game.get_score(), game.get_time());
        //String message = "Ready for next level?\n(best_score: " + game.get_score() + ", time: " + game.get_time()+")";
        String message =
                "Try again?\n" +
                        "*** score: "+game.get_score()+", time: "+game.get_time()+" ***\n" +
                        "* play_count: "+level_info.play_count+"\n"+
                        "* best_score: "+level_info.best_score+"\n"+
                        "* avg_score: "+level_info.avg_score+"\n"+
                        "* best_time: "+level_info.best_time+"\n"+
                        "* avg_time: "+level_info.avg_time+"\n"+
                        "* worst_time: "+level_info.worst_time;

        RelativeLayout main = (RelativeLayout) findViewById(R.id.main);
        DialogView dialog = new DialogView(this, R.drawable.dialog_good_egg, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity
                finish();
                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                intent.putExtra("level_num", level_num + 1);
                startActivity(intent);
            }
        });

        dialog.show(main, "You win!", false);


    }
}
