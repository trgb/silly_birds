package org.electroblanzifor.mnemo;

import android.graphics.Point;

/**
 * Created by ayal on 29/09/13.
 */
public class BirdConfig {
    public final int rid;
    public int scale = 1;
    public Point anchor;

    public BirdConfig(int rid, Point anchor){
        this.rid = rid;
        this.anchor = anchor;
    }

    public BirdConfig(int rid, Point anchor, int scale){
        this.rid = rid;
        this.anchor = anchor;
        this.scale = scale;
    }
}
