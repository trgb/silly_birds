package org.electroblanzifor.mnemo;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by ayal on 05/10/13.
 */
public class DialogView extends RelativeLayout {
    private final GameActivity context;
    private final DialogInterface.OnClickListener mlistener;
    private View innerimage;

    public DialogView(GameActivity context, int innerimage_rid, DialogInterface.OnClickListener listener) {
        super(context);
        this.context = context;
        this.mlistener  = listener;
        // inner image:
        final int finnerimage_rid = innerimage_rid;
        this.innerimage = new ImageView(context){{
          setImageResource(finnerimage_rid);
        }};

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("WebViewDemo", "TOUCH dialog");
                int eventaction = event.getAction();
                //Log.i("WebViewDemo", "ON-TOUCH!, " + eventaction + " , sound >> " + ((CardView)v).stimulus.resourceName);
                if(eventaction == MotionEvent.ACTION_UP){
                    // Do what you want
                    return true;
                }
                if(eventaction == 0) {
                    try {
                        mlistener.onClick(null,0);
                        return true;
                    }
                    catch (Exception exx) {
                        Log.i("WebViewDemo", "DIALOG TOUCH EXCEPTION " + exx);
                    }

                }
                return false;
            }
        });

    }

    public void show(final RelativeLayout main, String text, boolean withinner) {
        try {
            final DialogView that = this;
            Log.i("WebViewDemo", "showing dialog...");
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(main.getWidth() - 100, main.getHeight() / 4);
            params.leftMargin = 50;
            params.topMargin = 150;

            // text:
            RelativeLayout.LayoutParams textparams = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);


            textparams.leftMargin = 50;
            textparams.topMargin = 50;

            TextView tv = new TextView(context);
            tv.setText(text);
            tv.setTypeface(Typeface.SANS_SERIF);
            tv.setTextSize(16);
            tv.setTextColor(Color.WHITE);
            this.addView(tv, textparams);

            // background:
            setBackgroundResource(R.drawable.shape);
            main.addView(this, params);

            ScaleAnimation s = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, (main.getWidth() - 100) / 2, (main.getHeight() - 300) / 2);
            s.setDuration(600);
            s.setInterpolator(new BounceInterpolator());
            s.setFillEnabled(true);
            s.setFillAfter(true);
            s.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    context.scoreview.move_to_other_view(main, that);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            this.startAnimation(s);

            if (withinner) {
            // inner image:
            RelativeLayout.LayoutParams innerparams = new RelativeLayout.LayoutParams(150,192);
            innerparams.addRule(CENTER_HORIZONTAL);
            innerparams.addRule(CENTER_VERTICAL);
            innerparams.topMargin = 150;
            this.addView(innerimage, innerparams);
            s.setDuration(600);

            TranslateAnimation trans = new TranslateAnimation(0.0f, 0.0f, 0.0f, 100.0f);
            trans.setFillEnabled(true);
            trans.setFillAfter(true);
            trans.setInterpolator(new BounceSin());
            trans.setDuration(10000);
            innerimage.startAnimation(trans);
            }
        }
        catch(Exception exx){
            Log.i("WebViewDemo", "view show Exception " +  exx);
        }

    }




}
