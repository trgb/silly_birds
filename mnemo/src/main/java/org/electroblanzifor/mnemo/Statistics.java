package org.electroblanzifor.mnemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**************************************************************************************************/

/**
 * Created by Boob on 30/09/13.
 */
public class Statistics
{
    public static final String LEVEL_STATS_NAME = "level_stats";

    private static Statistics _instance;

    private final SharedPreferences level_stats;

    private Statistics(Context context) {
        // initialize "preferences" file
        level_stats = context.getSharedPreferences(LEVEL_STATS_NAME, Context.MODE_PRIVATE);
    }

    public static Statistics instance(Context context) {
        if (_instance == null) {
            _instance = new Statistics(context);
        }

        return _instance;

    }

    /**********************************************************************************************/

    public class LevelInfo {
        public int play_count;
        public int best_score;
        public double avg_score;
        public double best_time;
        public double avg_time;
        public double worst_time;
        public LevelInfo() {
            play_count = 0;
            best_score = -999;
            avg_score = best_time = avg_time = worst_time = 0;
        }
        public LevelInfo(String json) throws JSONException {
            JSONObject json_obj = new JSONObject(json);
            play_count = json_obj.getInt("play_count");
            best_score = json_obj.getInt("best_score");
            avg_score = json_obj.getDouble("avg_score");
            best_time = json_obj.getDouble("best_time");
            avg_time = json_obj.getDouble("avg_time");
            worst_time = json_obj.getDouble("worst_time");
        }

        public String to_json(String level_name) {
            return "{\"level_name\":\""+level_name+"\""+
                    ",\"play_count\":"+play_count+
                    ",\"best_score\":"+best_score+
                    ",\"avg_score\":"+avg_score+
                    ",\"best_time\":"+best_time+
                    ",\"avg_time\":"+avg_time+
                    ",\"worst_time\":"+worst_time+"}";
        }

        public String to_json() {
            return "{\"play_count\":"+play_count+
                   ",\"best_score\":"+best_score+
                   ",\"avg_score\":"+avg_score+
                   ",\"best_time\":"+best_time+
                   ",\"avg_time\":"+avg_time+
                   ",\"worst_time\":"+worst_time+"}";
        }
    }

    public LevelInfo get_level_info(int id) {
        if (!level_stats.contains(""+id))
            return new LevelInfo();

        String level_json = level_stats.getString(""+id, "?");
        try {
            return new LevelInfo(level_json);
        } catch (JSONException e) {
            Log.e("silly_birds", "JSON EXCEPTION FOR LEVEL \"" + id + "\"");
            //e.printStackTrace();
            return new LevelInfo();
        }
    }

    public LevelInfo update_level(int id, int score, double time) {
        LevelInfo info = get_level_info(id);
        if (info.play_count == 0)
        {
            info.play_count = 1;
            info.best_score = score;
            info.avg_score = score;
            info.best_time = time;
            info.avg_time = time;
            info.worst_time = time;
        }
        else {
            info.best_score = Math.max(info.best_score, score);
            info.avg_score = (info.avg_score*info.play_count + score) / (info.play_count+1);
            info.best_time = Math.min(info.best_time, time);
            info.avg_time = (info.avg_time*info.play_count + time) / (info.play_count+1);
            info.worst_time = Math.max(info.worst_time, time);
            info.play_count += 1;
        }

        SharedPreferences.Editor editor = level_stats.edit();
        editor.putString(""+id, info.to_json());
        editor.commit();

        return info;
    }
}
