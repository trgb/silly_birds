package org.electroblanzifor.mnemo;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;

/**
 * Created by ayal on 03/10/13.
 */
public class Tweener {
    private final Object tweens[];

    public Tweener(Object[] tweens){
        this.tweens = tweens;
    }

    public void run(View v, int tweendex){
        if (tweendex > tweens.length - 1) {
            return;
        }
        float[] tween = (float[])tweens[tweendex];

        final View fv = v;
        final int ftweendex = tweendex;

        ScaleAnimation s = new ScaleAnimation(tween[0], tween[1], tween[2], tween[3]);
        s.setDuration((int)tween[4]);
        s.setInterpolator(new CustomBounceInterpolator());
        s.setFillEnabled(true);
        s.setFillAfter(true);

        s.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //run(fv, ftweendex + 1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(s);
    }
}
